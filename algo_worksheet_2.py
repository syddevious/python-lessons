from random import randint
from collections import deque

size = randint(20, 100)
lbase = [randint(-100, 100) for i in range(size)]
lcheck = lbase.copy()
lcheck.sort()
l1 = lbase.copy()
l2 = lbase.copy()
l3 = lbase.copy()

tgraph = {
    "a": ["b", "c", "d"],
    "b": ["a", "e", "d"],
    "c": ["d", "a"],
    "d": ["a", "b", "c"],
    "e": ["b"]
}

tree = {
    "a": ["b", "c"],
    "b": ["d", "e"],
    "c": ["f", "g"],
    "d": ["h", "i"],
    "e": ["j", "k"],
    "f": ["l"],
    "g": [],
    "h": [],
    "i": [],
    "j": [],
    "k": [],
    "l": []
}

dgraph = {
    's': {'a': 2, 'b': 1},
    'a': {'s': 3, 'b': 4, 'c': 8},
    'b': {'s': 4, 'a': 2, 'd': 2},
    'c': {'a': 2, 'd': 7, 't': 4},
    'd': {'b': 1, 'c': 11, 't': 5},
    't': {'c': 3, 'd': 5}
}

para = ['you', 'your', 'cannot', 'can', 'cause', 'convey', 'corruptions', 'composition', 'create', 'first', 'for',
        'foundation', 'functions', 'familial', 'from', 'information', 'in', 'into', 'idea', 'ideas', 'is', 'important',
        'put', 'paragraphs', 'paragraph', 'particular', 'paper', 'process', 'progression', 'working', 'whole', 'where',
        'whichever', 'which', 'what', 'with', 'will', 'argument', 'are', 'as', 'an', 'and', 'any', 'about', 'all', 'a',
        'be', 'begins', 'begin', 'between', 'better', 'before', 'building', 'brainstorming', 'have', 'some', 'supports',
        'suppose', 'stage', 'statement', 'should', 'seed', 'other', 'of', 'organic', 'or', 'on', 'one', 'done',
        'development', 'develop', 'decide', 'decision', 'determine', 'many', 'mind', 'must', 'most', 'natural',
        'recurrent', 'relationship', 'relationships', 'related', 'reader', 'remind', 'keep', 'known', 'that', 'thesis',
        'the', 'there', 'this', 'techniques', 'trying', 'to', 'like', 'else', 'every', 'each', 'germination']
test_trie = {'y': {'o': {'u': {'*': {}, 'r': {'*': {}}}}},
             'c': {'a': {'n': {'n': {'o': {'t': {'*': {}}}}, '*': {}}, 'u': {'s': {'e': {'*': {}}}}},
                   'o': {'n': {'v': {'e': {'y': {'*': {}}}}},
                         'r': {'r': {'u': {'p': {'t': {'i': {'o': {'n': {'s': {'*': {}}}}}}}}}},
                         'm': {'p': {'o': {'s': {'i': {'t': {'i': {'o': {'n': {'*': {}}}}}}}}}}},
                   'r': {'e': {'a': {'t': {'e': {'*': {}}}}}}}, 'f': {'i': {'r': {'s': {'t': {'*': {}}}}},
                                                                      'o': {'r': {'*': {}}, 'u': {'n': {'d': {'a': {
                                                                          't': {'i': {'o': {'n': {'*': {}}}}}}}}}},
                                                                      'u': {'n': {'c': {
                                                                          't': {'i': {'o': {'n': {'s': {'*': {}}}}}}}}},
                                                                      'a': {'m': {
                                                                          'i': {'l': {'i': {'a': {'l': {'*': {}}}}}}}},
                                                                      'r': {'o': {'m': {'*': {}}}}}, 'i': {
        'n': {'f': {'o': {'r': {'m': {'a': {'t': {'i': {'o': {'n': {'*': {}}}}}}}}}}, '*': {}, 't': {'o': {'*': {}}}},
        'd': {'e': {'a': {'*': {}, 's': {'*': {}}}}}, 's': {'*': {}},
        'm': {'p': {'o': {'r': {'t': {'a': {'n': {'t': {'*': {}}}}}}}}}}, 'p': {'u': {'t': {'*': {}}}, 'a': {
        'r': {'a': {'g': {'r': {'a': {'p': {'h': {'s': {'*': {}}, '*': {}}}}}}},
              't': {'i': {'c': {'u': {'l': {'a': {'r': {'*': {}}}}}}}}}, 'p': {'e': {'r': {'*': {}}}}}, 'r': {
        'o': {'c': {'e': {'s': {'s': {'*': {}}}}}, 'g': {'r': {'e': {'s': {'s': {'i': {'o': {'n': {'*': {}}}}}}}}}}}},
             'w': {'o': {'r': {'k': {'i': {'n': {'g': {'*': {}}}}}}},
                   'h': {'o': {'l': {'e': {'*': {}}}}, 'e': {'r': {'e': {'*': {}}}},
                         'i': {'c': {'h': {'e': {'v': {'e': {'r': {'*': {}}}}}, '*': {}}}}, 'a': {'t': {'*': {}}}},
                   'i': {'t': {'h': {'*': {}}}, 'l': {'l': {'*': {}}}}},
             'a': {'r': {'g': {'u': {'m': {'e': {'n': {'t': {'*': {}}}}}}}, 'e': {'*': {}}}, 's': {'*': {}},
                   'n': {'*': {}, 'd': {'*': {}}, 'y': {'*': {}}}, 'b': {'o': {'u': {'t': {'*': {}}}}},
                   'l': {'l': {'*': {}}}, '*': {}}, 'b': {'e': {'*': {}, 'g': {'i': {'n': {'s': {'*': {}}, '*': {}}}},
                                                                't': {'w': {'e': {'e': {'n': {'*': {}}}}},
                                                                      't': {'e': {'r': {'*': {}}}}},
                                                                'f': {'o': {'r': {'e': {'*': {}}}}}},
                                                          'u': {'i': {'l': {'d': {'i': {'n': {'g': {'*': {}}}}}}}},
                                                          'r': {'a': {'i': {'n': {'s': {'t': {
                                                              'o': {'r': {'m': {'i': {'n': {'g': {'*': {}}}}}}}}}}}}}},
             'h': {'a': {'v': {'e': {'*': {}}}}}, 's': {'o': {'m': {'e': {'*': {}}}}, 'u': {
        'p': {'p': {'o': {'r': {'t': {'s': {'*': {}}}}, 's': {'e': {'*': {}}}}}}}, 't': {
        'a': {'g': {'e': {'*': {}}}, 't': {'e': {'m': {'e': {'n': {'t': {'*': {}}}}}}}}},
                                                        'h': {'o': {'u': {'l': {'d': {'*': {}}}}}},
                                                        'e': {'e': {'d': {'*': {}}}}},
             'o': {'t': {'h': {'e': {'r': {'*': {}}}}}, 'f': {'*': {}},
                   'r': {'g': {'a': {'n': {'i': {'c': {'*': {}}}}}}, '*': {}}, 'n': {'*': {}, 'e': {'*': {}}}},
             'd': {'o': {'n': {'e': {'*': {}}}},
                   'e': {'v': {'e': {'l': {'o': {'p': {'m': {'e': {'n': {'t': {'*': {}}}}}, '*': {}}}}}},
                         'c': {'i': {'d': {'e': {'*': {}}}, 's': {'i': {'o': {'n': {'*': {}}}}}}},
                         't': {'e': {'r': {'m': {'i': {'n': {'e': {'*': {}}}}}}}}}},
             'm': {'a': {'n': {'y': {'*': {}}}}, 'i': {'n': {'d': {'*': {}}}}, 'u': {'s': {'t': {'*': {}}}},
                   'o': {'s': {'t': {'*': {}}}}}, 'n': {'a': {'t': {'u': {'r': {'a': {'l': {'*': {}}}}}}}}, 'r': {
        'e': {'c': {'u': {'r': {'r': {'e': {'n': {'t': {'*': {}}}}}}}}, 'l': {'a': {
            't': {'i': {'o': {'n': {'s': {'h': {'i': {'p': {'*': {}, 's': {'*': {}}}}}}}}}, 'e': {'d': {'*': {}}}}}},
              'a': {'d': {'e': {'r': {'*': {}}}}}, 'm': {'i': {'n': {'d': {'*': {}}}}}}},
             'k': {'e': {'e': {'p': {'*': {}}}}, 'n': {'o': {'w': {'n': {'*': {}}}}}}, 't': {
        'h': {'a': {'t': {'*': {}}}, 'e': {'s': {'i': {'s': {'*': {}}}}, '*': {}, 'r': {'e': {'*': {}}}},
              'i': {'s': {'*': {}}}}, 'e': {'c': {'h': {'n': {'i': {'q': {'u': {'e': {'s': {'*': {}}}}}}}}}},
        'r': {'y': {'i': {'n': {'g': {'*': {}}}}}}, 'o': {'*': {}}}, 'l': {'i': {'k': {'e': {'*': {}}}}},
             'e': {'l': {'s': {'e': {'*': {}}}}, 'v': {'e': {'r': {'y': {'*': {}}}}}, 'a': {'c': {'h': {'*': {}}}}},
             'g': {'e': {'r': {'m': {'i': {'n': {'a': {'t': {'i': {'o': {'n': {'*': {}}}}}}}}}}}}}


class LinkedNode:
    def __init__(self, data=None, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right


root = LinkedNode('root')
rnode = root
for i in range(10):
    new = LinkedNode(i, rnode)
    rnode.right = new
    rnode = new

heap_data = [9, 6, 3, 17, 4,  1,  10,  8, 15, 13, 5]

# DO NOT MODIFY ABOVE!!!

# Worksheet below


def insertion_sort(arr):
    """
    time: O(n^2)
    space: O(1)

    TODO: write insertion sort algo.
    insertion sort is sort in place.
    Fill out time and space complexity above in big O.
    do not return anything. Sort original array.
    """
    for i in range(len(arr)):
        j = i-1
        key = arr[i]
        while j >= 0 and arr[j] > key:
            arr[j+1] = arr[j]
            j -= 1
        arr[j+1] = key


def merge_sort(arr):
    """
   time: O(nlogn)
   space: O(n)

   TODO: write merge sort algo.
   Fill out time and space complexity above in big O.
   do not return anything. Sort original array.
    """
    if len(arr) > 1:
        mid = len(arr)//2
        l = arr[:mid]
        r = arr[mid:]

        merge_sort(l)
        merge_sort(r)

        i = j = k = 0
        while i < len(l) and j < len(r):
            if l[i] < r[j]:
                arr[k] = l[i]
                i += 1
            else:
                arr[k] = r[j]
                j += 1
            k += 1

        while i < len(l):
            arr[k] = l[i]
            i += 1
            k += 1

        while j < len(r):
            arr[k] = r[j]
            j += 1
            k += 1


def quick_sort(arr, low, high):
    """
       time: O(nlogn)
       space: O(1)

       TODO: write quick sort algo.
       quick sort is sort in place.
       Fill out time and space complexity above in big O.
       do not return anything. Sort original array.

       additional paramters: high -> initial value is len(arr)-1 {size of original array}.
                            low -> initial value is 0 {first index of array}
    """
    if low < high:
        pivot = arr[high]

        i = low - 1
        for j in range(low, high):
            if arr[j] < pivot:
                i += 1
                arr[i], arr[j] = arr[j], arr[i]
        arr[i + 1], arr[high] = arr[high], arr[i + 1]

        quick_sort(arr, low, i)
        quick_sort(arr, i + 2, high)


def binary_search(arr, value, low, high):
    """
       time: O(logn)
       space: O(1)

       TODO: write binary search algo.
       return true if value is found in array. false if not.
       Fill out time and space complexity above in big O.

        must be a sorted array.
       value ->  the value in the array, that you are searching for
       """
    if low < high:

        mid = (high-low)//2 + low

        if arr[mid] == value:
            return True
        elif arr[mid] < value:
            return binary_search(arr, value, mid+1, high)
        else:
            return binary_search(arr, value, low, mid-1)

    elif arr[high] == value:
        return True
    else:
        return False


def bfs(graph, root):
    """
   time:
   space:

   TODO: write breadth first search algo.
   return list of visited nodes in correct visitation order.
   Fill out time and space complexity above in big O.
    """
    q = deque([root])
    v = []
    while q:
        n = q.popleft()
        v.append(n)
        for node in graph[n]:
            if node not in v and node not in q:
                q.append(node)
    return v


def dfs(graph, node, v=[]):
    """
       time: depends on setup of tree or graph.
       space: O(n)

       TODO: write depth first search algo.
       return list of visited nodes in correct visitation order.
       Fill out time and space complexity above in big O.
       """
    v.append(node)
    for n in graph[node]:
        if n not in v:
            dfs(graph, n, v)


def dijkstra(graph, start, end):
    # setup base table.
    # need to track the shortest path to a node from start, and what the previous vertex that led here is.

    # initialize list of UNVISITED NODES

    # do below process until unvisted list is empty

    # find node with shortest distance currently that is still unvisited.
    # initial run should produce starting node.

    # do things with min node.
    # if the minnode distance + the weight of any of its neighboring nodes is less than the existing weight
    # of its neirhboring nodes, then update the shortest distance to neighboring node, and its previous vertex
    # that got it to this new shortest distance.

    # now that table is fully populated with shortest distance to each node (from the start),
    # and also with the previous node that gets to each nodes shortest_dist,
    # we can trace backwards through the table starting from the endpoint until we hit the start point.

    shortest_dist = {k: 9999999 for k in graph}
    parents = {k: '' for k in graph}
    unvisited = [k for k in graph]
    parents[start] = None
    shortest_dist[start] = 0

    while unvisited:
        min_node = None
        for i in range(len(unvisited)):
            if min_node is None:
                min_node = i
            elif shortest_dist[unvisited[i]] < shortest_dist[unvisited[min_node]]:
                min_node = i
        min_node = unvisited.pop(min_node)

        neighbors = graph[min_node]
        for node, weight in neighbors.items():
            if weight + shortest_dist[min_node] < shortest_dist[node]:
                shortest_dist[node] = weight + shortest_dist[min_node]
                parents[node] = min_node

    node = end
    path = deque([])
    while node:
        path.appendleft(node)
        node = parents[node]
    return path


def make_trie(words):
    tr = {}

    for word in words:
        node = tr
        for ch in word:
            node[ch] = node.get(ch, {})
            node = node[ch]
        node['*'] = {}
    return tr


def get_trie_words(tri):
    words = []
    for k, v in tri.items():
        if k != '*':
            for el in get_trie_words(v):
                words.append(k + el)
        else:
            words.append('')
    return words


def longest_common_substring(str1, str2):
    # write function to compare two strings and retrieve longest common sequence length. return integer.
    # sequence doesnt not have to be consecutive. But must be in order in both strings.
    # eg: Harry and sally share a common sub sequence of ay....
    m = len(str1)
    n = len(str2)
    matrix = [[0 for i in range(n + 1)] for i in range(m + 1)]

    for i in range(1, m + 1):
        for j in range(1, n + 1):
            if str1[i - 1] == str2[j - 1]:
                matrix[i][j] = matrix[i-1][j-1] + 1
            else:
                matrix[i][j] = max(matrix[i-1][j], matrix[i][j-1])

    return matrix[m][n]


def fibonacci(n):
    ns = [0, 1, 1]
    i = 2
    while i < n:
        ns.append(ns[i] + ns[i-1])
        i += 1
    return ns[n]


def reverse_linked_list(ll):
    end_node = ll
    while end_node.right:
        end_node = end_node.right

    start = end_node
    node = start
    while node:
        node.left, node.right = node.right, node.left
        node = node.right
    return start



# def max_heapify(data):
#     heap = []
#     last_non_leaf = len(data)//2 - 1
#     i = last_non_leaf
#     while i >= 0:
#
#
#
# def min_heapify(data):
#     heap = []
#     pass


# assertions below.
insertion_sort(l1)
assert l1 == lcheck
print("insertion sort is good")
merge_sort(l2)
assert l2 == lcheck
print("merge sort is good")
quick_sort(l3, 0, len(l3) - 1)
assert l3 == lcheck
print("quick sort is good")

notin = -100
for i in range(-100, 100):
    if i not in lbase:
        notin = i
        break
assert binary_search(lcheck, lbase[randint(0, size - 1)], 0, len(lcheck) - 1) == True
assert binary_search(lcheck, notin, 0, len(lcheck) - 1) == False
print("binary search is good")
assert bfs(tgraph, 'a') == "a,b,c,d,e".split(',')
assert bfs(tree, 'a') == "a,b,c,d,e,f,g,h,i,j,k,l".split(',')
print("breadth first search is good")

v1 = []
dfs(tgraph, 'b', v1)
assert v1 == "b,a,c,d,e".split(',')
v2 = []
dfs(tree, 'a', v2)
assert v2 == "a,b,d,h,i,e,j,k,c,f,l,g".split(',')
print("depth first search is good")

assert list(dijkstra(dgraph, 's', 't')) == ['s', 'b', 'd', 't']
assert list(dijkstra(dgraph, 'd', 'a')) == ['d', 'b', 'a']
print('dijkstras algo is good!')

trie = make_trie(para)
assert trie == test_trie
print("make trie is good!")
assert para == get_trie_words(test_trie)
print("get trie words is good!")

assert longest_common_substring('shinchan', 'noharaaa') == 3
print("using matrix to get length of longest common sequence is good!")


fib_checks = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711,
              28657, 46368, 75025, 121393, 196418, 317811]
ind_check = randint(0, len(fib_checks)-1)
assert fibonacci(ind_check) == fib_checks[ind_check]
print("fibonacci check is good!")


root = reverse_linked_list(root)
nodes = []
rnode = root
while rnode.right:
    nodes.append(rnode.data)
    rnode = rnode.right
assert nodes == [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
print("reverse linked list is good!")

# print(max_heapify(heap_data))
# print(min_heapify(heap_data))

print("Congratulations. youre done!")
