"""

CLASSES in PYTHON


- inheritance
- duck typing
- composite classes
-

"""


# linked list
#  obj1.next -> obj2.next -> obj3.next -> obj4.next -> obj5.next ->


# obj = obj1
#
# obj.next.next

class Hammer:
    class_attr = 50

    def __init__(self, weight, color):
        self.weight = weight
        self.color = color

    @staticmethod
    def hit(force):
        return "hit with force"

    def physics_hit(self, force):
        return self.weight * force

    @property
    def description(self):
        return f"hammer is of weight: {self.weight}, and color: {self.color}"

    @classmethod
    def anothermethod(cls, x):
        return cls.class_attr + x


class LinkedNode:

    def __init__(self, user_var1, next=None):
        self.data = user_var1
        self.next = next


test1 = LinkedNode("h") # set starting node
obj = test1 # set current working obj to starting node
for i in range(10): # for each number up to 10 do the following
    newobj = LinkedNode(obj.data + "f") # create a new INSTANCE of Linked Node. where self.data == the current working object + "f"

    obj.next = newobj # set the current working objecst self.next to reference the new object
    obj = newobj # set current working object to reference the new object


### DO NOT TOUCH ABOVE ###

def reverse_list(start):

    # write code here. dont touch return or function definition.

    return start

### DO NOT TOUCH BELOW ###


if __name__ == "__main__":
    test1 = reverse_list(test1)
    assert test1.data == "hffffffffff", "reverse linked list failed"
    print("this is working! good job!")






